import java.util.ArrayList;

public class Tauler {
  private int files;
  private int columnes;
  private int inicialXCela;
  private int inicialYCela;
  private int ampleCela;
  private int altCela;
  private int torn;
  Cela[][] celes;

  /**
  * Inicialitza el tauler
  */
  
  public Tauler(int files, int columnes) {
    this.files = files;
    this.columnes = columnes;
    this.ampleCela = 130;
    this.altCela = 130;
    this.inicialXCela = 325;
    this.inicialYCela = 195;
  }
   
  /**
  * Crea el Tauler
  */
  public void CrearTauler () {
    celes = new Cela[files][columnes];
    int tmpX = inicialXCela, tmpY = inicialYCela;
    int tipus = 1;    
    for (int i = 0; i < files; i++) {
       for (int j = 0; j < columnes; j++) {
         celes[i][j] = new Cela(ampleCela, altCela, tmpX+=ampleCela, tmpY, tipus);        
       }
       tmpX = inicialXCela;
       tmpY = inicialYCela+(altCela*(i+1));
     }
  }
  
  /**
  * Mostra el Tauler
  */
  public void MostrarTauler () {
    for (Cela[] fila : celes) {
       for(Cela c : fila) {
         c.dibuixar();
       }
    }    
  }
  
  // Getters && Setters
  public int getTorn() {
    return torn;
  }
  public void setTorn(int torn) {
    this.torn = torn;
  }
  public int getFiles() {
    return files;
  }
  public void setFiles(int files) {
    this.files = files;
  }
  public int getColumnes() {
    return columnes;
  }
  public void setColumnes(int columnes) {
    this.columnes = columnes;
  }
  public int getInicialXCela() {
    return inicialXCela;
  }
  public void setInicialXCela(int inicialXCela) {
    this.inicialXCela = inicialXCela;
  }
  public int getInicialYCela() {
    return inicialYCela;
  }
  public void setInicialYCela(int inicialYCela) {
    this.inicialYCela = inicialYCela;
  }
  public int getAmpleCela() {
    return ampleCela;
  }
  public void setAmpleCela(int ampleCela) {
    this.ampleCela = ampleCela;
  }
  public int getAltCela() {
    return altCela;
  }
  public void setAltCela(int altCela) {
    this.altCela = altCela;
  }
  public int getMaxX() {
    return files * inicialXCela;
  }
  public int getMaxY() {
    return columnes * inicialYCela;  
  }
  public boolean checkPlena(int mouseX, int mouseY) {
    int i = 0;
    torn++;
    for (Cela[] fila : celes) {
      for (Cela cela : fila) {
        
        // Comprovar que es la cela clicada
        if (mouseX >= cela.getxInicial() && mouseX <= cela.getMaxCelaXRang() 
        && mouseY >= cela.getyInicial() && mouseY <= cela.getMaxCelaYRang()){
          if (cela.getStatus() != 0) {
            return true;
          }                      
        }        
        i++;
      }
    }
    return false;
  }
  
  public void omplirCela(int mouseX, int mouseY, int mouseStatus) {
    for (Cela[] fila : celes) {
      
      for (Cela cela : fila) {  
        
        // Comprovar que es la cela clicada
        if (mouseX >= cela.getxInicial() && mouseX <= cela.getMaxCelaXRang() 
        && mouseY >= cela.getyInicial() && mouseY <= cela.getMaxCelaYRang()){
            cela.setStatus(mouseStatus+1);
            cela.update();
            break;
        }        
      }      
    }
    
  }
  
  public void checkWinner() {
    // 0 player win
    if(celes[0][0].getStatus() == 1 && celes[1][0].getStatus() == 1 && celes[2][0].getStatus() == 1){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 1 ha guanyat.",450,650);
    } else if(celes[0][1].getStatus() == 1 && celes[1][1].getStatus() == 1 && celes[2][1].getStatus() == 1){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 1 ha guanyat.",450,650);
    } else if(celes[0][2].getStatus() == 1 && celes[1][2].getStatus() == 1 && celes[2][2].getStatus() == 1){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 1 ha guanyat.",450,650);
    } else if(celes[0][0].getStatus() == 1 && celes[0][1].getStatus() == 1 && celes[0][2].getStatus() == 1){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 1 ha guanyat.",450,650);
    } else if(celes[1][0].getStatus() == 1 && celes[1][1].getStatus() == 1 && celes[1][2].getStatus() == 1){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 1 ha guanyat.",450,650);
    } else if(celes[2][0].getStatus() == 1 && celes[2][1].getStatus() == 1 && celes[2][2].getStatus() == 1){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 1 ha guanyat.",450,650);
    } else if(celes[0][0].getStatus() == 1 && celes[1][1].getStatus() == 1 && celes[2][2].getStatus() == 1){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 1 ha guanyat.",450,650);
    } else if(celes[2][0].getStatus() == 1 && celes[1][1].getStatus() == 1 && celes[0][2].getStatus() == 1){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 1 ha guanyat.",450,650);
    }







    // X player win
    if(celes[0][0].getStatus() == 2 && celes[1][0].getStatus() == 2 && celes[2][0].getStatus() == 2){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 2 ha guanyat.",450,650);
    } else if(celes[0][1].getStatus() == 2 && celes[1][1].getStatus() == 2 && celes[2][1].getStatus() == 2){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 2 ha guanyat.",450,650);
    } else if(celes[0][2].getStatus() == 2 && celes[1][2].getStatus() == 2 && celes[2][2].getStatus() == 2){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 2 ha guanyat.",450,650);
    } else if(celes[0][0].getStatus() == 2 && celes[0][1].getStatus() == 2 && celes[0][2].getStatus() == 2){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 2 ha guanyat.",450,650);
    } else if(celes[1][0].getStatus() == 2 && celes[1][1].getStatus() == 2 && celes[1][2].getStatus() == 2){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 2 ha guanyat.",450,650);
    } else if(celes[2][0].getStatus() == 2 && celes[2][1].getStatus() == 2 && celes[2][2].getStatus() == 2){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 2 ha guanyat.",450,650);
    } else if(celes[0][0].getStatus() == 2 && celes[1][1].getStatus() == 2 && celes[2][2].getStatus() == 2){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 2 ha guanyat.",450,650);
    } else if(celes[2][0].getStatus() == 2 && celes[1][1].getStatus() == 2 && celes[0][2].getStatus() == 2){
      fill(255);
      rect(400,600, 500, 100);
      PFont f;
      f = createFont("Arial",16,true);
      fill(0);
      textFont(f,24);                 
      text("El jugador 2 ha guanyat.",450,650);
    }



  }
  public void mostrarTorn() {
    rect(400,50, 500, 100);
    PFont f;                           // STEP 1 Declare PFont variable
    f = createFont("Arial",16,true); // STEP 2 Create Font
    fill(0);
    textFont(f,24);                  // STEP 3 Specify font to be used
    text("Torn del jugador " + ((torn % 2) + 1),450,100);
  }
}
