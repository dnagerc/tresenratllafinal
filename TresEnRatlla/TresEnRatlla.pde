int mouseStatus = -1;
int colocades = 0;
int boardStatus = -1;
int indexCercle = 0;
int indexImatge = 4;

// Utilitzem imatges per als cursors
PImage X, O, wallpaper;
Tauler t;
ArrayList<Fitxa> fitxes = new ArrayList<Fitxa>();

void setup() {
  size(1280, 720);
  // Wallpaper
  wallpaper = loadImage("wallpaper.jpg"); 
  image(wallpaper, 0, 0, 1280, 720);

  // Cursor
  cursor(CROSS);
  
  // Player Image used for Mouse && Fitxa X
  X = loadImage("cross.jpg");
  X.resize(100, 100);

  // Player Image used for Mouse
  O = loadImage("circle.png");
  
  // TableBoard Configuration  
  t = new Tauler(3, 3);
  t.CrearTauler();
  t.MostrarTauler();
  t.mostrarTorn();
  // Crear fitxes 
  int tipusFitxa = 0;
  int ampleFitxa = 100;
  int altFitxa = 100;
  int xInicialFitxa = 100;
  int yInicialFitxa = 100;
  int tmpX = xInicialFitxa;
  int tmpY = yInicialFitxa;
  for (int i = 0; i < 2; i++) {
     for (int j = 0; j < 4; j++) {
         fitxes.add(new Fitxa(ampleFitxa, altFitxa, tmpX+(ampleFitxa*2), tmpY+=(altFitxa+10), tipusFitxa));
     }
     tmpX = xInicialFitxa+650;
     tmpY = yInicialFitxa-(altFitxa/2)+10;
     tipusFitxa++;
   }

  // Mostrar fitxes
  for (Fitxa f: fitxes) {
    f.dibuixar();
  }
}

void draw() {
  // print(mouseStatus);
//  clear();
//  wallpaper = loadImage("wallpaper.jpg"); 
 // image(wallpaper, 0, 0, 1280, 720);
 // t.MostrarTauler();
  //for (Fitxa f: fitxes) {
   // f.dibuixar();
  //}

  // Print cercle to the mouse
  if (mouseStatus == 0) {
      // Per millorar el rendiment, es fa servir una imatge per al cursor, ja que sino s'hauria de repintar tot
      // ellipse(mouseX, mouseY, 100, 100);
      cursor(O);
   //   fitxes.remove(0);
     // mouseStatus = -1;
  }

  // Print cross to the mouse
  if (mouseStatus == 1) {
      cursor(X);    
//      fitxes.remove(0);
 //     mouseStatus = -1;
  }
  
  // Print default pointer to the mouse
  if (mouseStatus == 2) {
      // clear mouse Event
      cursor(CROSS);
      mouseStatus = -1;
  }
  
  if (boardStatus == 1) {
    boardStatus = -1;
  } else if (boardStatus == 0) {
    boardStatus = -1;
  }
}

void mousePressed() {
  // If  
//  mouseStatus = 1;
  // Minim nombre de fitxes per a que hi hagi guanyador
  
  // Clic sobre fitxa
  for (Fitxa f : fitxes) {
        switch (f.getTipus()) {
          // Cercle
          case 0:
            if (mouseX >= f.getxInicial()-(f.getAmple()/2) && mouseX < f.getMaxX()-(f.getAmple()/2) 
            && mouseY > f.getyInicial()-(f.getAlt()/2) && mouseY < f.getMaxY()-(f.getAlt()/2)) {
              mouseStatus = 0;
            }
            break;
          // X
          case 1:
            if (mouseX >= f.getxInicial() && mouseX < f.getMaxX() && mouseY > f.getyInicial() && mouseY < f.getMaxY()) {
              mouseStatus = 1;
            }
            break;
        }
  }
  
  
  // Clic sobre el Tauler
  if (mouseX >= t.getInicialXCela() && mouseX < t.getMaxX() && mouseY > t.getInicialYCela() && mouseY < t.getMaxY()){
     boardStatus = 1;
     if(t.checkPlena(mouseX, mouseY) == false){
       t.omplirCela(mouseX, mouseY, mouseStatus);
       colocades++;
       fill(255);
       t.mostrarTorn();
     }
  // No s'ha clicat tauler
  } else {
     boardStatus = 0;
  }
  t.checkWinner();
}
