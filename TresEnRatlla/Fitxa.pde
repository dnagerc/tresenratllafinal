public class Fitxa {
  private int ample;
  private int alt;
  private int xInicial;
  private int yInicial;
  private int tipus;
  
  // 0 es mostra, 1 s'oculta
//  private int status;
  
  
  public Fitxa(int ample, int alt, int xInicial, int yInicial, int tipus) {
    this.ample = ample;
    this.alt = alt;
    this.xInicial = xInicial;
    this.yInicial = yInicial;
    this.tipus = tipus;
    print(this.tipus);
//    this.status = 0;
  }
  
  public void dibuixar() {
    // Cercle
    if (tipus == 0) {
      fill(#30ed86);
      ellipse(xInicial, yInicial, ample, alt);   
    } else if (tipus == 1) {
      image(loadImage("cross.jpg"),xInicial, yInicial, ample, alt);
    }
  }
  
  public int getAmple() {
    return ample;
  }
  public void setAmple(int ample) {
    this.ample = ample;
  }
  public int getAlt() {
    return alt;
  }
  public void setAlt(int alt) {
    this.alt = alt;
  }
  public int getxInicial() {
    return xInicial;
  }
  public void setxInicial(int xInicial) {
    this.xInicial = xInicial;
  }
  public int getyInicial() {
    return yInicial;
  }
  public void setyInicial(int yInicial) {
    this.yInicial = yInicial;
  }
  public int getTipus() {
    return tipus;
  }
  public void setTipus(int tipus) {
    this.tipus = tipus;
  }
  public int getMaxX() {
    return xInicial+ample;
  }
  public int getMaxY() {
    return yInicial+alt;
  }
}
