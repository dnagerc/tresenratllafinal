public class Cela {
  private int ample;
  private int alt;
  private int xInicial;
  private int yInicial;
  private int tipus;
  private int status;
  
  public Cela(int ample, int alt, int xInicial, int yInicial, int tipus) {
    this.ample = ample;
    this.alt = alt;
    this.xInicial = xInicial;
    this.yInicial = yInicial;
    this.tipus = tipus;
    this.status = 0;
  }
  
  public int getAmple() {
    return ample;
  }

  public void setAmple(int ample) {
    this.ample = ample;
  }

  public int getAlt() {
    return alt;
  }

  public void setAlt(int alt) {
    this.alt = alt;
  }

  public int getxInicial() {
    return xInicial;
  }

  public void setxInicial(int xInicial) {
    this.xInicial = xInicial;
  }

  public int getyInicial() {
    return yInicial;
  }

  public void setyInicial(int yInicial) {
    this.yInicial = yInicial;
  }

  public int getTipus() {
    return tipus;
  }

  public void setTipus(int tipus) {
    this.tipus = tipus;
  }
  
  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public void dibuixar() {
      rect(xInicial, yInicial, ample, alt); 
  }

  public int getMaxCelaXRang() {
    return xInicial + ample;
  } 
  
  public int getMaxCelaYRang() {
    return yInicial + alt;
  } 

  public void update() {
    switch(status){
      // cercle
      case 1: 
          fill(#30ed86);
          ellipse(xInicial+(ample/2), yInicial+(alt/2), ample, alt);
          break;
      // imatge
      case 2: 
        image(loadImage("cross.jpg"),xInicial+5, yInicial+10, ample-10, alt-10);
        break;
      default:
        break;
  }
  }
}
